﻿namespace bl2_exp_tracker_v2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartTrackerButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Label_expUsing = new System.Windows.Forms.Label();
            this.Label_Remaining = new System.Windows.Forms.Label();
            this.Label_exp = new System.Windows.Forms.Label();
            this.chkBox_StreamerMode = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartTrackerButton
            // 
            this.StartTrackerButton.Location = new System.Drawing.Point(12, 12);
            this.StartTrackerButton.Name = "StartTrackerButton";
            this.StartTrackerButton.Size = new System.Drawing.Size(92, 23);
            this.StartTrackerButton.TabIndex = 0;
            this.StartTrackerButton.Text = "Start tracking";
            this.StartTrackerButton.UseVisualStyleBackColor = true;
            this.StartTrackerButton.Click += new System.EventHandler(this.StartTrackerButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.Label_expUsing);
            this.panel1.Controls.Add(this.Label_Remaining);
            this.panel1.Controls.Add(this.Label_exp);
            this.panel1.Location = new System.Drawing.Point(13, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 70);
            this.panel1.TabIndex = 1;
            // 
            // Label_expUsing
            // 
            this.Label_expUsing.AutoSize = true;
            this.Label_expUsing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Label_expUsing.Location = new System.Drawing.Point(3, 25);
            this.Label_expUsing.Name = "Label_expUsing";
            this.Label_expUsing.Size = new System.Drawing.Size(101, 16);
            this.Label_expUsing.TabIndex = 4;
            this.Label_expUsing.Text = "Currently using: ";
            // 
            // Label_Remaining
            // 
            this.Label_Remaining.AutoSize = true;
            this.Label_Remaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Label_Remaining.Location = new System.Drawing.Point(3, 41);
            this.Label_Remaining.Name = "Label_Remaining";
            this.Label_Remaining.Size = new System.Drawing.Size(124, 25);
            this.Label_Remaining.TabIndex = 3;
            this.Label_Remaining.Text = "To level up:";
            // 
            // Label_exp
            // 
            this.Label_exp.AutoSize = true;
            this.Label_exp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Label_exp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label_exp.Location = new System.Drawing.Point(3, 0);
            this.Label_exp.Name = "Label_exp";
            this.Label_exp.Size = new System.Drawing.Size(137, 25);
            this.Label_exp.TabIndex = 0;
            this.Label_exp.Text = "Current EXP:";
            // 
            // chkBox_StreamerMode
            // 
            this.chkBox_StreamerMode.AutoSize = true;
            this.chkBox_StreamerMode.Location = new System.Drawing.Point(111, 13);
            this.chkBox_StreamerMode.Name = "chkBox_StreamerMode";
            this.chkBox_StreamerMode.Size = new System.Drawing.Size(142, 17);
            this.chkBox_StreamerMode.TabIndex = 2;
            this.chkBox_StreamerMode.Text = "Green BG(For streaming)";
            this.chkBox_StreamerMode.UseVisualStyleBackColor = true;
            this.chkBox_StreamerMode.CheckedChanged += new System.EventHandler(this.chkBox_StreamerMode_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 124);
            this.Controls.Add(this.chkBox_StreamerMode);
            this.Controls.Add(this.StartTrackerButton);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "BL2 XP Tracker";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartTrackerButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Label_exp;
        private System.Windows.Forms.Label Label_Remaining;
        private System.Windows.Forms.Label Label_expUsing;
        private System.Windows.Forms.CheckBox chkBox_StreamerMode;
    }
}

