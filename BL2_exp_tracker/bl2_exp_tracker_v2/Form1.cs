﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace bl2_exp_tracker_v2
{
    public partial class Form1 : Form
    {
        #region Configurable values

        //CONFIGURE THESE VALUES ONLY
        public static string ProcessName = "Borderlands2"; //Process name. Normally no need to edit that
        public static string ExeName = ProcessName + ".exe"; //exe name
        public static int ThreadSleepTime = 1000; //Time for RunTracker thread to sleep after every cycle.
        public static int MaxLevel = 80; //Maximum level. 80 as of Commander Lilith DLC release.

        #endregion

        #region Memory addresses and offsets
        //Base addresses
        public static int bSavegameExp = 0x00D4F660;          //Savegame's exp. Final value is int32
        public static int bLiveExpMilestone = 0x0164D198; //Live exp milestone to next level. Final value is int32
        public static int bLiveExpSinglePlayer = 0x0164C458; //Live exp base pointer of singleplayer
        public static int bLiveExpMultiplayer = 0x0164D198;         //Live exp base pointer of co-op

        //Offsets
        public static int[] oLiveSinglePlayer =
        { 0x24, 0xA4, 0xB58, 0x6C }; //Live game exp for singleplayer offsets

        public static int[] oLiveCoopPlayer =
        { 0x3EC, 0xA4, 0xB58, 0x6C }; //Live game exp for coop offsets

        public static int[] oLiveExpMilestone =
        { 0xA4, 0x1BC, 0x28, 0x194, 0x25C }; //Live game's exp milestone offsets

        int[] oSavegame =
        { 0xC }; //Offset for savegame exp

        #endregion

        #region Form methods

        /// <summary>
        /// Form constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        #region Form controls

        /// <summary>
        /// Button to start tracking
        /// </summary>
        private void StartTrackerButton_Click(object sender, EventArgs e)
        {
            tracker.Start();//Starts the tracker thread
            StartTrackerButton.Text = "Tracking..."; //Changes text to...
            StartTrackerButton.Enabled = false; //Disables button
        }

        /// <summary>
        /// Activates when you check the Streamer mode check box
        /// </summary>
        private void chkBox_StreamerMode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBox_StreamerMode.Checked) //if checked
                panel1.BackColor = Color.Green; //makes panel green
            else //if not checked
                panel1.BackColor = Color.White; //makes panel white
        }

        #endregion

        System.Threading.Thread tracker; //Tracker method cast

        /// <summary>
        /// Activates when form is loaded in.
        /// </summary>
        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing); //Creates a handler for window closing
            tracker = new System.Threading.Thread(RunTracker); //Makes a new background thread for the tracker method
        }

        /// <summary>
        /// Activates when form is being closed
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            tracker.Abort(); //Kills the tracking thread
        }

        #endregion

        #region Other methods

        /// <summary>
        /// The main tracking method
        /// </summary>
        public void RunTracker()
        {
            //We find the process for the game
            Process process = FindProcess(ProcessName);

            //If the button has been pressed(basicly before this is even run)
            while(!StartTrackerButton.Enabled)
            {
                //We get all the values from memory
                int vSavegame = GetValueInt32(process, (IntPtr)bSavegameExp, oSavegame);
                int vExpMilestone = GetValueInt32(process, (IntPtr)bLiveExpMilestone, oLiveExpMilestone);
                int vLiveExpSolo = (int)GetValueFloat(process, (IntPtr)bLiveExpSinglePlayer, oLiveSinglePlayer);
                int vLiveExpCoop = (int)GetValueFloat(process, (IntPtr)bLiveExpMultiplayer, oLiveCoopPlayer);

                //We decide which exp value to use.
                //When we do, we do three things with it:
                //1.Update the value in form
                //2.Show in form which value we're using
                //3.If available, we calculate the remaining exp to level up(in game it is available)
                switch (FindWhichValueUsable(vSavegame, vLiveExpSolo, vLiveExpCoop)) {
                    case 3: //If live game coop value usable
                        UpdateFormLabel(Label_exp, "Current EXP: " + vLiveExpCoop);
                        UpdateFormLabel(Label_expUsing, "Currently using: Live Co-Op value");
                        UpdateFormLabel(Label_Remaining, "To level up: " + FindRemainingExp(vLiveExpCoop, vExpMilestone));
                        break;
                    case 2: //If live game solo value usable
                        UpdateFormLabel(Label_exp, "Current EXP: " + vLiveExpSolo);
                        UpdateFormLabel(Label_expUsing, "Currently using: Live Single Player value");
                        UpdateFormLabel(Label_Remaining, "To level up: " + FindRemainingExp(vSavegame, vExpMilestone));
                        break;
                    case 1: //If savegame value usable
                        UpdateFormLabel(Label_exp, "Current EXP: " + vSavegame);
                        UpdateFormLabel(Label_expUsing, "Currently using: Savegame value");
                        UpdateFormLabel(Label_Remaining, "To level up: Unavailable"); //Since the exp milestone is available only ingame, we don't calculate
                        break;
                    case 0: //If none usable
                        UpdateFormLabel(Label_exp, "Current EXP: Unavailable"); //We show 'unavailable' in all values
                        UpdateFormLabel(Label_expUsing, "Currently using: Unavailable");
                        UpdateFormLabel(Label_Remaining, "To level up: Unavailable");
                        break;
                }
            }
        }

        /// <summary>
        /// Find the process of the game(usually only one)
        /// </summary>
        /// <param name="processName">(string)Name of the process.</param>
        /// <returns>(Process)Process of the game.</returns>
        public Process FindProcess(string processName)
        {
            Process[] processes = Process.GetProcessesByName(processName);//Get all processes by their name(only one in game's case)
            if(processes.Count() <= 0) //If game's process is not found
            {
                MessageBox.Show("Start the game and try running this program again!", "No BL2 Game found!"); //We throw this message box
                this.Close(); //Close the form, which effectively kills the program
                return null; //And return nothing, since it's not used anyway
            }
            else //If it's found, however
            {
                return processes.FirstOrDefault();//We return one and only process.
            }
        }

        /// <summary>
        /// Gets a 32-bit integer value from memory.
        /// </summary>
        /// <param name="process">Process from which we're reading</param>
        /// <param name="bAddress">Base address</param>
        /// <param name="oAddress">Offsets for looking</param>
        /// <returns>Found value</returns>
        public int GetValueInt32(Process process, IntPtr bAddress, int[] oAddress)
        {
            using(Memory memReader = new Memory(process))//Using the memory library
            {
                try //Try catching an error within this. It might not find the value or address
                {
                    IntPtr address = memReader.GetAddress(ExeName, bAddress, oAddress); //Finds the correct address from pointers
                    int value = memReader.ReadInt32(address); //Reads the said address
                    return value; //Returns the found value
                }
                catch(System.AccessViolationException) //If value is not found, this will happen
                {
                    return 0; //Return a 0, which shouldn't happen in normal operation
                }
            }
        }

        /// <summary>
        /// Gets a float value from memory.
        /// </summary>
        /// <param name="process">Process from which we're reading</param>
        /// <param name="bAddress">Base address</param>
        /// <param name="oAddress">Offets for pointers</param>
        /// <returns>Found value</returns>
        public float GetValueFloat(Process process, IntPtr bAddress, int[] oAddress)
        {
            using (Memory memReader = new Memory(process))//While accessing the memory
            {
                try //Safeguard to catch an error, if the value is not accessible
                {
                    IntPtr address = memReader.GetAddress(ExeName, bAddress, oAddress); //Gets the address from pointers
                    return memReader.ReadFloat(address); //Returns the read value
                }
                catch (System.AccessViolationException) //If error is caught
                {
                    return 0; //Return a 0, which won't happen in normal play
                }
            }
        }

        /// <summary>
        /// Universal method to update label texts.
        /// </summary>
        /// <param name="label">Label which we're updating</param>
        /// <param name="text">Text for the label</param>
        public void UpdateFormLabel(Label label, string text)
        {
            if(label.InvokeRequired) //If this method is called from a non-host thread(i.e. RunTracker thread)
                label.Invoke((MethodInvoker)delegate () //We invoke it to be called from host thread
                {
                    UpdateFormLabel(label, text);//And do the same thing.
                });
            else //If this is called from host thread(i.e. after being invoked by client thread)
            {
                label.Text = text; //We change the text of the label.
            }
        }

        /// <summary>
        /// Finds, which value is usable from all 3 acquired values.
        /// </summary>
        /// <param name="vSaveGame">Save game's value.</param>
        /// <param name="vLiveSolo">Singleplayer's live value.</param>
        /// <param name="vLiveCoop">Coop live value.</param>
        /// <returns>3 if coop vlaue; 2 if solo value; 1 if savegame value; 0 if neither</returns>
        public int FindWhichValueUsable(int vSaveGame, int vLiveSolo, int vLiveCoop)
        {
            if (vLiveCoop > 0)//Simple if else statements. If Co-op live value is above 0
                return 3; //Return a 3.
            else if (vLiveSolo > 0) //Otherwise, if solo live value is above 0
                return 2; //Return a 2.
            else if (vSaveGame > 0) //Otherwise, if savegame's value is above 0
                return 1; //Return a 1.
            else return 0; //If none of the said values are available for use, return a 0.
        }

        /// <summary>
        /// Helper method to calculate remaining XP.
        /// </summary>
        /// <param name="liveExperience">Experience value from which we're calculating.</param>
        /// <param name="xpMilestone">Total experience to get a level up. Usually available in-game.</param>
        /// <returns>XP needed to level up.</returns>
        public int FindRemainingExp(int liveExperience, int xpMilestone)
        {
            return xpMilestone - liveExperience; //Returns a simple calculation.
        }

        #endregion

    }
}
