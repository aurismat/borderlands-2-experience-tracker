# Borderlands 2 Experience tracker

A Proof-Of-Concept .NET C# program that tracks Borderlands 2 experience live.
Works in latest version 1.8.5 *should*

**Why?**

Problem with the normal BL2 HUD is, once you reach endgame to grind to the final levels, you want to have a numerical feedback of your grinding.
With this program, I'm aiming to fix that.
Obviously, it's not a DirectX calls to write an overlay, it's a WinForms window, so if you're playing fullscreen, you'll want to use another window or pin it.

**How?**

I initially intended to use [VAMemory](https://vivid-abstractions.net/logical/programming/vamemory-c-memory-class-net-3-5/) library, as it's easy to use and doesn't require elevated privileges, but unfortunately I couldn't manage to get it to read pointers from game's base module address(think Cheat Engine).

Then I tried using erfg12's [memory.dll](https://github.com/erfg12/memory.dll) library, as it made my life easier. It does mean this program needs admin priveleges, however.

Finally, I stumbled upon Matej Tomčík's [video](https://www.youtube.com/watch?v=TWUK-fK7xD8), which had the libraries included needed to get this done without admin access.
So this is what I settled on, and it works well.

I have also included the Cheat Engine cheat tables, so you can confirm these work for you. If not, the game has been updated(as of last update of this project, latest version is 1.8.5). You'll have to find new pointers for the addresses.

While using this in-game, the tracker might say you're reading from Co-op value even though you are playing singleplayer or hosting co-op. *THIS IS NORMAL*. It reads from best available value, and Co-op value is the same as singleplayer value(needs confirmation).

Now, I do realise that this might have some serious security issues or whatever. This is mostly a **botched** project, so if you feel like you can fix it, fork and go ahead.

**What works so far**

*   Tracks your characters EXP from whichever value is accessible;
*   Detects if memory is not accessible;
*   Shows both level and XP remaining to level up.(Calculated from reading memory value for total xp to next level, so won't work in main menu);


**To-do**

* [x]  Make a 'XP to next level' counter or something like that **DONE**
* [ ]  Catch other possible exceptions
